(defproject LatestFMCLI "1.0.0-SNAPSHOT"
  :description "FIXME: write"
  :dependencies [[org.clojure/clojure "1.2.0"]
                 [org.clojure/clojure-contrib "1.2.0"]
                 [net.roarsoftware/last.fm-bindings "1.0"]]
  :dev-dependencies [[org.clojars.autre/lein-vimclojure "1.0.0"]]
  :repositories {"xebia-mvn" "http://os.xebia.com/maven2"}
  :main LatestFMCLI.core)
