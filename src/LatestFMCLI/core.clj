(ns LatestFMCLI.core
  (:import (net.roarsoftware.lastfm User))
  (:gen-class))

(def lastfm-api-key "85e...")

(def lastfm-user-name "sv...")

(defn top-tracks
  "Get the top played tracks of a Last.FM user"
  [user-name api-key]
 (User/getTopTracks user-name api-key))

(defn track-to-str
  "Format a Last.FM track as a pretty printed string"
  [track]
  (let [ track-name (.getName track)
          artist-name (.getArtist track)]
    (str track-name " by " artist-name)))

(defn number-a-sequence
  "Appends an increasing number to the elements of  sequence, starting with 1"
  [seeq]
  (map-indexed #(str (+ 1 %1) " " %2) seeq))

(defn to-html
  "converts a list of string items to HTML, by appending a 
   header and footer and adding linebreaks to each item"
  [str-seeq]
  (let [ header "<html><body>"
          footer "</body></html>"]
    (str header (reduce str (map #(str % "<br/>") str-seeq)) footer )))

(defn -main
  []
  (to-html (number-a-sequence (map #(track-to-str %) (top-tracks lastfm-user-name lastfm-api-key)))))
